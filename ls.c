#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <sys/stat.h>

// une macro d�finition
// attention ce n'est pas une fonction!
#define LISIBLE_PAR_L_UTILISATEUR(m) (m&S_IRUSR)

int main(int argc,char *argv[]) {
  struct dirent *de;
  struct stat infos;
  // Ouvertuyre du r�pertoire courant en lecture
  DIR *dir = opendir(".");
  if (dir==NULL) { perror("probleme avec ."); exit(1); }
  // Lecture it�rative d'entr�e de r�pertoire
  while ( (de=readdir(dir)) != NULL) {
    // R�cup�ration des m�ta-informations associ�es � l'in�ud associ� � la r�f�rence donn�e
    if (stat(de->d_name,&infos)!=-1) {
      // Affichage de diff�rentes informations
      printf("%lld %c%c%c%c %d %4d %12lld %s\n",
             infos.st_ino,
             S_ISDIR(infos.st_mode)?'d':'-',
             LISIBLE_PAR_L_UTILISATEUR(infos.st_mode)?'r':'-',
             (infos.st_mode&S_IWUSR)?'w':'-',
             (infos.st_mode&S_IXUSR)?'x':'-',
             infos.st_uid,
             infos.st_nlink,
             infos.st_size,
             de->d_name);
    }
  }
  // Lib�ration des ressources associ�es
  closedir(dir);

  exit(0);
}
